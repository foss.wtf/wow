--PvP Icons SetAlpha
PlayerPVPIcon:SetAlpha(0)
TargetFrameTextureFramePVPIcon:SetAlpha(0)
FocusFrameTextureFramePVPIcon:SetAlpha(0)

--CombatText overlay on Player and Pet
PlayerHitIndicator:SetText(nil)
PlayerHitIndicator.SetText = function() end

PetHitIndicator:SetText(nil)
PetHitIndicator.SetText = function() end

--Group Number Hide
PlayerFrameGroupIndicator.Show = function() return end

--Target and Player Frame Scale
PlayerFrame:SetScale(1.1)
TargetFrame:SetScale(1.1)
FocusFrame:SetScale(1)

--castbar scale
-- CastingBarFrame:ClearAllPoints()
-- CastingBarFrame:SetPoint("CENTER",UIParent,"CENTER", 0, -235)
-- CastingBarFrame.SetPoint = function() end
-- CastingBarFrame:SetScale(1.0)

TargetFrameSpellBar:ClearAllPoints()
TargetFrameSpellBar:SetPoint("CENTER", UIParent, "CENTER", 0, -135)
TargetFrameSpellBar.SetPoint = function() end
TargetFrameSpellBar:SetScale(1)

--target castbar above frame
-- TargetFrameSpellBar:ClearAllPoints()
-- TargetFrameSpellBar:SetPoint("BOTTOM", TargetFrame, "TOP", -15, 0)
-- TargetFrameSpellBar.SetPoint = function() end

--Repair and Crap Sell
local g = CreateFrame("Frame")
g:RegisterEvent("MERCHANT_SHOW")

g:SetScript("OnEvent", function()  
        local bag, slot
        for bag = 0, 4 do
                for slot = 0, GetContainerNumSlots(bag) do
                        local link = GetContainerItemLink(bag, slot)
                        if link and (select(3, GetItemInfo(link)) == 0) then
                                UseContainerItem(bag, slot)
                        end
                end
        end

        if(CanMerchantRepair()) then
                local cost = GetRepairAllCost()
                if cost > 0 then
                        local money = GetMoney()
                        if IsInGuild() then
                                local guildMoney = GetGuildBankWithdrawMoney()
                                if guildMoney > GetGuildBankMoney() then
                                        guildMoney = GetGuildBankMoney()
                                end
                                if guildMoney > cost and CanGuildBankRepair() then
                                        RepairAllItems(1)
                                        print(format("|cfff07100Repair cost covered by G-Bank: %.1fg|r", cost * 0.0001))
                                        return
                                end
                        end
                        if money > cost then
                                RepairAllItems()
                                print(format("|cffead000Repair cost: %.1fg|r", cost * 0.0001))
                        else
                                print("Not enough gold to cover the repair cost.")
                        end
                end
        end
end)

--Chat Shadow Font
for i = 1, 7 do
    local chat = _G["ChatFrame"..i]
    local font, size = chat:GetFont()
    chat:SetFont(font, size, "THINOUTLINE")
    chat:SetShadowOffset(0, 0)
    chat:SetShadowColor(0, 0, 0, 0)
end


--Flashy Spellsteal border
-- hooksecurefunc("TargetFrame_UpdateAuras", function(s)
    -- for i = 1, MAX_TARGET_BUFFS do
        -- _, _, ic, _, dT = UnitBuff(s.unit, i)
        -- if(ic and (not s.maxBuffs or i<=s.maxBuffs)) then
            -- fS=_G[s:GetName()..'Buff'..i..'Stealable']
            -- if(UnitIsEnemy(PlayerFrame.unit, s.unit) and dT=='Magic') then
                -- fS:Show()
            -- else
                -- fS:Hide()
            -- end
        -- end
    -- end
-- end)


--outofrange red buttons
-- hooksecurefunc("ActionButton_OnEvent",function(self, event, ...)
-- if ( event == "PLAYER_TARGET_CHANGED" ) then
-- self.newTimer = self.rangeTimer
-- end
-- end)
-- hooksecurefunc("ActionButton_UpdateUsable",function(self)
-- local icon = _G[self:GetName().."Icon"]
-- local valid = IsActionInRange(self.action)
-- if ( valid == false ) then
-- icon:SetVertexColor(1.0, 0.1, 0.1)
-- end
-- end)
-- hooksecurefunc("ActionButton_OnUpdate",function(self, elapsed)
-- local rangeTimer = self.newTimer
-- if ( rangeTimer ) then
-- rangeTimer = rangeTimer - elapsed
-- if ( rangeTimer <= 0 ) then
-- ActionButton_UpdateUsable(self)
-- rangeTimer = TOOLTIP_UPDATE_TIME
-- end
-- self.newTimer = rangeTimer
-- end
-- end)


--Minimap stuff
MinimapZoomIn:Hide()
MinimapZoomOut:Hide()
Minimap:EnableMouseWheel(true)
Minimap:SetScript('OnMouseWheel', function(self, delta)
    if delta > 0 then
        Minimap_ZoomIn()
    else
        Minimap_ZoomOut()
    end
end)
MiniMapTracking:ClearAllPoints()
MiniMapTracking:SetPoint("TOPRIGHT", -26, 7)

--slash commands
SlashCmdList["CLCE"] = function() CombatLogClearEntries() end
SLASH_CLCE1 = "/clc"

SlashCmdList["TICKET"] = function() ToggleHelpFrame() end
SLASH_TICKET1 = "/gm"

SlashCmdList["READYCHECK"] = function() DoReadyCheck() end
SLASH_READYCHECK1 = '/rc'

SlashCmdList["CHECKROLE"] = function() InitiateRolePoll() end
SLASH_CHECKROLE1 = '/cr'


--hide hotkeys
-- for i=1, 12 do
    -- _G["ActionButton"..i.."HotKey"]:SetAlpha(0) -- main bar
    -- _G["MultiBarBottomRightButton"..i.."HotKey"]:SetAlpha(0) -- bottom right bar
    -- _G["MultiBarBottomLeftButton"..i.."HotKey"]:SetAlpha(0) -- bottom left bar
    -- _G["MultiBarRightButton"..i.."HotKey"]:SetAlpha(0) -- right bar
    -- _G["MultiBarLeftButton"..i.."HotKey"]:SetAlpha(0) -- left bar
-- end

-- --hide macro labels
-- for i=1, 12 do
    -- _G["ActionButton"..i.."Name"]:SetAlpha(0) -- main bar
    -- _G["MultiBarBottomRightButton"..i.."Name"]:SetAlpha(0) -- bottom right bar
    -- _G["MultiBarBottomLeftButton"..i.."Name"]:SetAlpha(0) -- bottom left bar
    -- _G["MultiBarRightButton"..i.."Name"]:SetAlpha(0) -- right bar
    -- _G["MultiBarLeftButton"..i.."Name"]:SetAlpha(0) -- left bar
-- end

-- Dark Textures
 local frame=CreateFrame("Frame")
 frame:RegisterEvent("ADDON_LOADED")

 frame:SetScript("OnEvent", function(self, event, addon)
          if (addon == "Blizzard_TimeManager") then
                 for i, v in pairs({PlayerFrameTexture, TargetFrameTextureFrameTexture, PetFrameTexture, PartyMemberFrame1Texture, PartyMemberFrame2Texture, PartyMemberFrame3Texture, PartyMemberFrame4Texture,
                          PartyMemberFrame1PetFrameTexture, PartyMemberFrame2PetFrameTexture, PartyMemberFrame3PetFrameTexture, PartyMemberFrame4PetFrameTexture, FocusFrameTextureFrameTexture,
                          TargetFrameToTTextureFrameTexture, FocusFrameToTTextureFrameTexture, BonusActionBarFrameTexture0, BonusActionBarFrameTexture1, BonusActionBarFrameTexture2, BonusActionBarFrameTexture3,
                          BonusActionBarFrameTexture4, MainMenuBarTexture0, MainMenuBarTexture1, MainMenuBarTexture2, MainMenuBarTexture3, MainMenuMaxLevelBar0, MainMenuMaxLevelBar1, MainMenuMaxLevelBar2,
                          MainMenuMaxLevelBar3, MinimapBorder, CastingBarFrameBorder, FocusFrameSpellBarBorder, TargetFrameSpellBarBorder, MiniMapTrackingButtonBorder, MiniMapLFGFrameBorder, MiniMapBattlefieldBorder,
                          MiniMapMailBorder, MinimapBorderTop,
                          select(1, TimeManagerClockButton:GetRegions())
                 }) do
                         v:SetVertexColor(.4, .4, .4)
                  end

                 for i,v in pairs({ select(2, TimeManagerClockButton:GetRegions()) }) do
                        v:SetVertexColor(1, 1, 1)
                  end

                  self:UnregisterEvent("ADDON_LOADED")
                  frame:SetScript("OnEvent", nil)
         end
 end)
 for i, v in pairs({ MainMenuBarLeftEndCap, MainMenuBarRightEndCap }) do
         v:SetVertexColor(.35, .35, .35)
end