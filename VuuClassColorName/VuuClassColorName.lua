 local frame = CreateFrame("FRAME")
 frame:RegisterEvent("GROUP_ROSTER_UPDATE")
 frame:RegisterEvent("PLAYER_TARGET_CHANGED")
 frame:RegisterEvent("PLAYER_FOCUS_CHANGED")
 frame:RegisterEvent("UNIT_FACTION")

 local function eventHandler(self, event, ...)
         if UnitIsPlayer("target") then
                 c = RAID_CLASS_COLORS[select(2, UnitClass("target"))]
                 TargetFrameNameBackground:SetVertexColor(c.r, c.g, c.b)
         end
         if UnitIsPlayer("focus") then
                 c = RAID_CLASS_COLORS[select(2, UnitClass("focus"))]
                 FocusFrameNameBackground:SetVertexColor(c.r, c.g, c.b)
         end
end

frame:SetScript("OnEvent", eventHandler)

for _, BarTextures in pairs({TargetFrameNameBackground, FocusFrameNameBackground}) do
         BarTextures:SetTexture("Interface\\TargetingFrame\\UI-StatusBar")
end

-- Option to make the background of the name black instead of friendly/hostile coloring. Comment the code above out if you want to use this here.
-- TargetFrameNameBackground:SetColorTexture(0,0,0)